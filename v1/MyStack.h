#ifndef __MY_STACK__
#define __MY_STACK__

template<typename StackType>
class MyStack {
	private:
	StackType * el;					// ������� �������
	MyStack * nextEl;				// ��������� �������

	public:
	/* init */
	MyStack(StackType elem = 0, MyStack * next = nullptr) {
		el = new StackType(elem);
		nextEl = next;
	};


	/* manipulate */
	bool push(StackType & elem) {
		if (el == nullptr) {
			try {
				nextEl = nullptr;
				el = new StackType(elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}

		} else {
			try {
				nextEl = new MyStack(*el, nextEl);
				el = new StackType(elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}
		}
		return true;
	};
	bool pushP(StackType * elem) {
		if (el == nullptr) {
			try {
				nextEl = nullptr;
				el = new StackType(*elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}

		} else {
			try {
				nextEl = new MyStack(*el, nextEl);
				el = new StackType(*elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}
		}
		return true;
	};
	StackType pop() {
		StackType frac = *el;
		MyStack * next = nextEl;
		if (next) {
			el = next->el;
			nextEl = next->nextEl;
		} else {
			el = nullptr;
			nextEl = nullptr;
		}
		return frac;
	};
	void clear() {
		if (el) delete el;
		if (nextEl) delete nextEl;
		el = nullptr;
		nextEl = nullptr;
	}
	int length() {
		int res = 0;
		MyStack * temp = this;
		while (temp->nextEl) { res++; temp = temp->nextEl; }
		return res + 1;
	}
	void print() {
		MyStack * temp = this;
		while (temp->nextEl) { cout << *(temp->el) << " , "; temp = temp->nextEl; }
		return;
	};

	/* get & set */
	const StackType * getEl() {
		return el;
	};
	void setEl(StackType el) {
		el = &frac;
		return;
	}
	MyStack * getNextEl() {
		return nextEl;
	}


	/* desctructor */
	~MyStack() {
		if (el) delete el;
		if (nextEl) delete nextEl;
	};
};

#endif // __MY_STACK__
