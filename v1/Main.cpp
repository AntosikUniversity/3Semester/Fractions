#include <iostream>
#include <fstream>	
#include "Class_Fraction.h"
#include "Functions_Helpers.h"
#include "MyStack.h"

using namespace std;

const char* FileInputName = "input.txt";

int main() {
	ifstream FileInput;
	FileInput.open(FileInputName, ios::in);

	char c = ' ', _c = ' ';
	char nowChar = ' ', topChar = ' ';
	char Number[10] = { 0 }, *NumberPtr = Number;

	fraction Prev(0), Now(0), Result(0);
	MyStack<char> operators('(');
	MyStack<fraction> fractions(fraction(0));

	while (!FileInput.eof()) {
		FileInput >> c;
		if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '(' || c == ')' || c == ';') {
			if (isdigit(_c)) {
				fractions.push(fraction(GornerScheme(Number, 10)));
				*NumberPtr = 0; NumberPtr = Number; memset(Number, 0, strlen(Number));
			}
			if (c == '(') {
				operators.push(c);
			} else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%') {
				if (Priority(c) <= Priority(*operators.getEl())) {
					Now = fractions.pop(); Prev = fractions.pop();
					switch (nowChar = operators.pop()) {
						case '+': Result = Prev + Now; std::cout << '\n' << Prev << " + " << Now << " = " << Result; break;
						case '-': Result = Prev - Now; std::cout << '\n' << Prev << " - " << Now << " = " << Result; break;
						case '*': Result = Prev * Now; std::cout << '\n' << Prev << " * " << Now << " = " << Result; break;
						case '/': Result = Prev / Now; std::cout << '\n' << Prev << " / " << Now << " = " << Result; break;
						case '%': Result = Prev % Now; std::cout << '\n' << Prev << " % " << Now << " = " << Result; break;
						default: printf("Undefined!"); break;
					}
					fractions.pushP(&Result);
				}
				operators.push(c);
			} else if (c == ')' || c == ';') {
				do {
					nowChar = operators.pop();
					int asd = operators.length();
					if (nowChar == '(' || nowChar == ' ') break;
					Now = fractions.pop(); Prev = fractions.pop();
					switch (nowChar) {
						case '+': Result = Prev + Now; std::cout << '\n' << Prev << " + " << Now << " = " << Result; break;
						case '-': Result = Prev - Now; std::cout << '\n' << Prev << " - " << Now << " = " << Result; break;
						case '*': Result = Prev * Now; std::cout << '\n' << Prev << " * " << Now << " = " << Result; break;
						case '/': Result = Prev / Now; std::cout << '\n' << Prev << " / " << Now << " = " << Result; break;
						case '%': Result = Prev % Now; std::cout << '\n' << Prev << " % " << Now << " = " << Result; break;
						default: printf("Undefined!"); break;
					}
					fractions.pushP(&Result);
				} while (nowChar != '(' && nowChar != ' ');
				if (nowChar == '(' && c == ';') {
					char cc = '(';
					operators.clear(); fractions.clear();
					operators.push(cc);
					fractions.pushP(new fraction(0));
					Prev = 0; Now = 0; Result = 0;
					std::cout << "\n\n-----------------------------------\n";
				}
			}
		} else if (isdigit(c)) {
			*NumberPtr++ = c;
		};
		_c = c;
	}

	FileInput.close();
	system("pause");
	return 0;
}