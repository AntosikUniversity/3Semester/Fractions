#ifndef __HELPERS__
#define __HELPERS__

#include <ctype.h>

int Priority(char oper);
int NOD(int first, int second);
int NOK(int first, int second);
int GornerScheme(char *num, int base);

#endif // __HELPERS__
