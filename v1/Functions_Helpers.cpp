#ifndef __HELPERS__
#include "Functions_Helpers.h"
#endif

int Priority(char oper) { // return Priority 1 - +-; 2 - */; 3 - (
	switch (oper) {
		case '(': case ')': return 0;
		case '+': case'-': return 1;
		case '*': case'/': case'%': return 2;
		default:
			break;
	}
};

int NOD(int first, int second) {
	int temp;
	if (first == second) return first;
	while (second) {
		temp = first % second;
		first = second;
		second = temp;
	}
	return first;
};

int NOK(int first, int second) {
	return (first * second) / NOD(first, second);
};

int GornerScheme(char *num, int base) {
	char *n = num;
	int Result = 0;
	while (*n) {
		Result = Result*base + (isdigit(*n) ? (*n++ - '0') : (toupper(*n++ - 'A' + 10)));
	}
	return Result;
};