#include <iostream>

#ifndef __FRACTION__
#define __FRACTION__

class fraction {
private:
	int numerator;		// числитель
	int denumerator;	// знаменатель

public:
	/* init */
	fraction(int num = 0, int denum = 1);
	fraction(const fraction & frac);


	/* get & set */
	void setNum(int num);
	void setDeNum(int denum);
	int getNum();
	int getDeNum();


	/* desctructor */
	~fraction();


	/* operators */
	friend fraction operator + (const fraction & first, const fraction & second);
	friend fraction operator - (const fraction & first, const fraction & second);
	friend fraction operator * (const fraction & first, const fraction & second);
	friend fraction operator / (const fraction & first, const fraction & second);
	friend fraction operator % (const fraction & first, const fraction & second);

	fraction & operator = (const fraction & second);
	fraction & operator += (const fraction & second);
	fraction & operator -= (const fraction & second);
	fraction & operator *= (const fraction & second);
	fraction & operator /= (const fraction & second);
	fraction & operator %= (const fraction & second);

	friend std::ostream & operator << (std::ostream & stream, const fraction & el);


	/* special */
	void reduce();
};

#endif // __FRACTION__
