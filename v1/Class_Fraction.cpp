#ifndef __FRACTION__
#include "Class_Fraction.h"
#endif

#ifndef __HELPERS__
#include "Functions_Helpers.h"
#endif // __HELPERS__

#include <iostream>
using namespace std;

/* init */
fraction::fraction(int num, int denum) : numerator(num), denumerator(denum) {}
fraction::fraction(const fraction & frac) : numerator(frac.numerator), denumerator(frac.denumerator) {}


/* get & set */
void fraction::setNum(int num) {
	numerator = num;
};
void fraction::setDeNum(int denum) {
	denumerator = denum;
};
int fraction::getNum() {
	return numerator;
};
int fraction::getDeNum() {
	return denumerator;
};


/* desctructor */
fraction::~fraction() {}


/* operators */
fraction operator + (const fraction & first, const fraction & second) {
	fraction *res = NULL;

	try {
		res = new fraction(first);
		*res += second;
	}
	catch (const bad_alloc & e) {
		cout << "ERROR: Memory allocation - " << e.what() << endl;
	}

	return *res;
}

fraction operator - (const fraction & first, const fraction & second) {
	fraction *res = NULL;

	try {
		res = new fraction(first);
		*res -= second;
	}
	catch (const bad_alloc & e) {
		cout << "ERROR: Memory allocation - " << e.what() << endl;
	}

	return *res;
}

fraction operator * (const fraction & first, const fraction & second) {
	fraction *res = NULL;

	try {
		res = new fraction(first);
		*res *= second;
	}
	catch (const bad_alloc & e) {
		cout << "ERROR: Memory allocation - " << e.what() << endl;
	}

	return *res;
}

fraction operator / (const fraction & first, const fraction & second) {
	fraction *res = NULL;

	try {
		res = new fraction(first);
		*res /= second;
	}
	catch (const bad_alloc & e) {
		cout << "ERROR: Memory allocation - " << e.what() << endl;
	}

	return *res;
}

fraction operator % (const fraction & first, const fraction & second) {
	fraction *res = NULL;

	try {
		res = new fraction(first);
		*res %= second;
	}
	catch (const bad_alloc & e) {
		cout << "ERROR: Memory allocation - " << e.what() << endl;
	}

	return *res;
}

fraction & fraction::operator = (const fraction & second) {
	this->numerator = second.numerator;
	this->denumerator = second.denumerator;
	this->reduce();
	return *this;
}

fraction & fraction::operator += (const fraction & second) {
	if (this->denumerator != second.denumerator) {
		int nod = NOK(this->denumerator, second.denumerator);
		this->numerator = (nod / second.denumerator) * this->numerator + (nod / this->denumerator) * second.numerator;
		this->denumerator = nod;
	}
	else {
		this->numerator += second.numerator;
	}
	this->reduce();
	return *this;
}

fraction & fraction::operator -= (const fraction & second) {
	if (this->denumerator != second.denumerator) {
		int nod = NOK(this->denumerator, second.denumerator);
		this->numerator = (nod / second.denumerator) * this->numerator - (nod / this->denumerator) * second.numerator;
		this->denumerator = nod;
	}
	else {
		this->numerator -= second.numerator;
	}
	this->reduce();
	return *this;
}

fraction & fraction::operator *= (const fraction & second) {
	this->numerator = this->numerator * second.numerator;
	this->denumerator = this->denumerator * second.denumerator;
	this->reduce();
	return *this;
}

fraction & fraction::operator /= (const fraction & second) {
	this->numerator = this->numerator * second.denumerator;
	this->denumerator = this->denumerator * second.numerator;
	this->reduce();
	return *this;
}

fraction & fraction::operator %= (const fraction & second) {
	this->numerator = this->numerator * second.denumerator;
	this->denumerator = this->denumerator * second.numerator;
	this->reduce();
	return *this;
}

ostream & operator << (ostream & stream, const fraction & el) {
	stream << '(' << el.numerator << '/' << el.denumerator << ')';
	return stream;
}


/* special */
void fraction::reduce() {
	int nok = NOD(numerator, denumerator);
	if (denumerator == 0 || nok == 0) {
		cout << "ERROR: Division by zero!" << endl;
	}
	if (denumerator < 0) {
		numerator = -numerator;
		denumerator = -denumerator;
	}
	numerator = numerator / nok;
	denumerator = denumerator / nok;
}